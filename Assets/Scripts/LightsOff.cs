﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightsOff : MonoBehaviour {
	public AudioClip electricStuff;
	public AudioClip clac;
	public AudioClip sbarabam;

	public GameObject fullScreenBlack;
	private AudioSource aSource;

	private Transform dungeonGeneratorT;

	public float minSecondsOfDay, maxSecondsOfDay;
	private float nextSecondsOfDay;

	public float minSecondsOfNight, maxSecondsOfNight;
	private float nextSecondsOfNight;

	public PlayerMovement pMovement;
	public PlayerHealth pHealth;
	private GlobalRandomRefs random;

	public enum TerrorState {
		Day,
		Night
	}
	public TerrorState actualState { get; private set; }

	private Light mainLight;
	private Camera mainCamera;

	public Color dayLightColor, nightLightColor;
	public Color depthDay, depthNight;

	void Awake() {
		aSource = audio;
		dungeonGeneratorT = GameObject.FindGameObjectWithTag("DungeonGenerator").transform;

		mainLight = GameObject.FindGameObjectWithTag("MainLight").GetComponent<Light>();
		mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
		random = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalRandomRefs>();

		mainLight.color = dayLightColor;
		mainCamera.backgroundColor = depthDay;

		Initialize();
	}

	private float timeCollector = 0;
	private bool timerPaused = false;
	void Update() {
		if(!timerPaused) {
			timeCollector += Time.deltaTime;

			if(actualState == TerrorState.Day) {
				if(timeCollector >= nextSecondsOfDay) {
					if(pMovement.whichRoomIAmIn.AreThereAnyMonsters())
						StartTheTerror();
					else
						timeCollector = 0;
				}
			}
			else {
				if(timeCollector >= nextSecondsOfNight) {
					StopTheTerror();
				}
			}
		}
	}

	private void Initialize() {
		timeCollector = 0;

		float difference = maxSecondsOfDay - minSecondsOfDay;
		nextSecondsOfDay = (float)((random.behaviourRandom.NextDouble() * difference) + minSecondsOfDay);
	}

	private void StartTheTerror() {
		timeCollector = 0;
		timerPaused = true;

		float difference = maxSecondsOfNight - minSecondsOfNight;
		nextSecondsOfNight = (float)((random.behaviourRandom.NextDouble() * difference) + minSecondsOfNight);

		StartCoroutine(TerrorSequence());
	}

	private void StopTheTerror() {
		timeCollector = 0;
		timerPaused = true;

		float difference = maxSecondsOfDay - minSecondsOfDay;
		nextSecondsOfDay = (float)((random.behaviourRandom.NextDouble() * difference) + minSecondsOfDay);

		StartCoroutine(UnterrorSequence());
	}

	private IEnumerator TerrorSequence() {
		if(actualState != TerrorState.Night) {
			List<NightmareController> nightmareControllers = new List<NightmareController>();
			foreach(Transform child in dungeonGeneratorT) {
				nightmareControllers.Add(child.GetComponent<NightmareController>());
			}

			for(int i = 0; i < nightmareControllers.Count; i++) {
				nightmareControllers[i].DisableAllDoorsColliders();
			}

			aSource.PlayOneShot(electricStuff);

			fullScreenBlack.SetActive(true);
			yield return new WaitForSeconds(0.12f);
			fullScreenBlack.SetActive(false);
			yield return new WaitForSeconds(0.14f);
			fullScreenBlack.SetActive(true);
			yield return new WaitForSeconds(0.18f);
			fullScreenBlack.SetActive(false);
			yield return new WaitForSeconds(0.16f);
			fullScreenBlack.SetActive(true);

			pHealth.GiftInvincibility(3f);

			for(int i = 0; i < nightmareControllers.Count; i++) {
				nightmareControllers[i].GoNightmare();
			}

			mainLight.color = nightLightColor;
			mainCamera.backgroundColor = depthNight;

			yield return new WaitForSeconds(0.40f);
			aSource.PlayOneShot(clac);
			yield return new WaitForSeconds(0.45f);
			aSource.PlayOneShot(clac);
			yield return new WaitForSeconds(0.8f);

			for(int i = 0; i < nightmareControllers.Count; i++) {
				nightmareControllers[i].CloseAllDoors();
			}

			aSource.PlayOneShot(sbarabam);
			fullScreenBlack.SetActive(false);

			actualState = TerrorState.Night;
			timerPaused = false;
		}
	}

	private IEnumerator UnterrorSequence() {
		if(actualState != TerrorState.Day) {
			List<NightmareController> nightmareControllers = new List<NightmareController>();
			foreach(Transform child in dungeonGeneratorT) {
				nightmareControllers.Add(child.GetComponent<NightmareController>());
			}

			aSource.PlayOneShot(clac);
			fullScreenBlack.SetActive(true);

			for(int i = 0; i < nightmareControllers.Count; i++) {
				nightmareControllers[i].GoDay();
			}

			yield return new WaitForSeconds(1f);

			for(int i = 0; i < nightmareControllers.Count; i++) {
				nightmareControllers[i].OpenAllDoors();
			}

			mainLight.color = dayLightColor;
			mainCamera.backgroundColor = depthDay;

			aSource.PlayOneShot(clac);
			fullScreenBlack.SetActive(false);
			
			actualState = TerrorState.Day;
			timerPaused = false;
		}
	}
}