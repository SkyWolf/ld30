﻿using UnityEngine;
using System;
using System.Collections;

public class GlobalRandomRefs : MonoBehaviour {
	public System.Random constructionRandom { get; private set; }
	public System.Random behaviourRandom { get; private set; }

	public void Awake() {
		constructionRandom = new System.Random(/*123*/);	//TODO: seed here if seeded run
		behaviourRandom = new System.Random();

		Time.timeScale = 1;
	}
}