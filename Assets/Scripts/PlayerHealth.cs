﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour {
	public int startingHealth;
	public int health { get; private set; }

	public SpriteRenderer theRenderer;
	private GUIController controller;

	void Awake() {
		health = startingHealth;
		controller = GameObject.FindGameObjectWithTag("GUI").GetComponent<GUIController>();
		controller.lifeCounter.RepresentHealth(Mathf.Max(0, health));

		Color theColor = theRenderer.sharedMaterial.color;
		theColor.a = 1;
		theRenderer.sharedMaterial.color = theColor;
	}

	private const float INVINCIBILITY_FRAMES = 1f;
	private float invincibilityTime;
	private float timeCollector; 
	void Update() {
		if(readyToBeHit == false) {
			timeCollector += Time.deltaTime;
			if(timeCollector >= invincibilityTime)
				readyToBeHit = true;
		}
	}

	public void GiftInvincibility(float howMuch) {
		timeCollector = 0;
		readyToBeHit = false;
		invincibilityTime = howMuch;

		StartCoroutine(Blink(invincibilityTime));
	}

	private bool readyToBeHit = true;
	void OnCollisionStay(Collision col) {
		GameObject go = col.collider.gameObject;

		if(go.layer == LayerMask.NameToLayer("Monster")) {
			if(readyToBeHit) {
				GiftInvincibility(INVINCIBILITY_FRAMES);

				health -= go.GetComponent<Monster>().currentDamage;
				controller.lifeCounter.RepresentHealth(Mathf.Max(0, health));

				if(health <= 0) {
					Time.timeScale = 0;
					controller.ShowGameOver();
				}
			}
		}
	}

	public float singleBlinkDuration;
	private float blinkDuration;
	private IEnumerator Blink(float totalBlinkDuration) {
		blinkDuration = 0;
		while(blinkDuration < totalBlinkDuration) {
			blinkDuration += singleBlinkDuration;

			Color theColor;
			theColor = theRenderer.sharedMaterial.color;
			theColor.a = 0;
			theRenderer.sharedMaterial.color = theColor;
			yield return new WaitForSeconds(singleBlinkDuration / 2);

			theColor = theRenderer.sharedMaterial.color;
			theColor.a = 1;
			theRenderer.sharedMaterial.color = theColor;
			yield return new WaitForSeconds(singleBlinkDuration / 2);
		}
	}
}
