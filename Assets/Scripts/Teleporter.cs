﻿using UnityEngine;
using System.Collections;

public class Teleporter : MonoBehaviour {
	private enum TeleportSteps {
		Ready,
		UpTo,
		CameraAndTeleport,
		DownFrom
	}
	private TeleportSteps actualStep;
	
	void Update() {
		switch(actualStep) {
			case TeleportSteps.UpTo:
				if(goingUp.actualState == RoomMovement.RoomStatus.Up) {
					actualStep = TeleportSteps.CameraAndTeleport;

					Vector3 newPosition = tpPosition.transform.position;
					newPosition.y = 0;
					character.position = newPosition;

					camera.GoDirection(dInfo.whichDoor);
				}
				break;
			case TeleportSteps.CameraAndTeleport:
				if(camera.actualState == CameraTransition.CameraState.Ready) {
					actualStep = TeleportSteps.DownFrom;

					goingDown.GoDown();
				}

				break;
			case TeleportSteps.DownFrom:
				if(goingDown.actualState == RoomMovement.RoomStatus.Down) {
					actualStep = TeleportSteps.Ready;
				}
				break;
		}
	}

	private RoomMovement goingUp;
	private RoomMovement goingDown;
	private Rigidbody character;
	private BoxCollider tpPosition;
	private DoorInformations dInfo;
	public CameraTransition camera;

	public void Teleport(Rigidbody player, RoomInformations from, RoomInformations to, BoxCollider whichDoor, DoorInformations dInformations) {
		goingUp = to.GetComponent<RoomMovement>();
		goingDown = from.GetComponent<RoomMovement>();

		character = player;
		tpPosition = whichDoor;
		dInfo = dInformations;

		actualStep = TeleportSteps.UpTo;
		goingUp.GoUp();
	}
}