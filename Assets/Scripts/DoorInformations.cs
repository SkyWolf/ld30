﻿using UnityEngine;
using System.Collections;

public class DoorInformations : MonoBehaviour {
	public enum DoorSide {
		Up, Left, Down, Right
	}

	public RoomInformations owner;
	public DoorSide whichDoor;
}
