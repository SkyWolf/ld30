﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HearthContainer : MonoBehaviour {
	public Sprite empty, half, full;
	private Image theImage;

	public void ShowEmpty() {
		if(!theImage)
			theImage = GetComponent<Image>();
		theImage.sprite = empty;
	}

	public void ShowHalf() {
		if(!theImage)
			theImage = GetComponent<Image>();
		theImage.sprite = half;
	}

	public void ShowFull() {
		if(!theImage)
			theImage = GetComponent<Image>();
		theImage.sprite = full;
	}
}
