﻿using UnityEngine;
using System.Collections;

public class ObjectSpotController : MonoBehaviour {
	public Transform activeObjectSpot;
	public Transform front, right, left, rear;

	public void ActivateSpotDir(int animDir) {
		if(animDir == 0 || animDir == 3)
			activeObjectSpot = front;
		else if(animDir == 1)
			activeObjectSpot = rear;
		else if(animDir == 2)
			activeObjectSpot = right;
		else if(animDir == 4)
			activeObjectSpot = left;
	}
}