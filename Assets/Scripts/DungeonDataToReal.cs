﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DungeonDataToReal : MonoBehaviour {
	public GameObject roomPrefab;
	private Transform myTransform;
	private RoomsList daMofoList;
	private GoalObjects gObjects;
	private GlobalRandomRefs random;

	public void Convert(DungeonGenerator generator) {
		if(!myTransform)
			myTransform = transform;
		if(!daMofoList || !random || !gObjects) { 
			GameObject global = GameObject.FindGameObjectWithTag("Global");
			daMofoList = global.GetComponent<RoomsList>();
			random = global.GetComponent<GlobalRandomRefs>();
			gObjects = global.GetComponent<GoalObjects>();
		}

		Transform startingT = GameObject.FindGameObjectWithTag("StartingRoom").transform;

		Room[,] dungeon = generator.dungeon;
		Hub startingRoom = null;

		int startingI = -1, startingJ = -1;

		for(int i = 0; i < generator.fixedDungeonRows; i++) {
			for(int j = 0; j < generator.fixedDungeonColumns; j++) {
				if(dungeon[i,j] is StartingRoom) {
					startingRoom = (StartingRoom)dungeon[i, j];
					startingI = i; startingJ = j;

					i = generator.fixedDungeonRows;
					j = generator.fixedDungeonColumns;
				}
			}
		}

		RoomInformations[,] tRooms = new RoomInformations[generator.fixedDungeonRows, generator.fixedDungeonColumns];
		RoomInformations iInformations = startingT.GetComponent<RoomInformations>();

			GameObject newRoomComponents = (GameObject)Instantiate(daMofoList.starting);
			Transform newRoomComponentsT = newRoomComponents.transform;
			
			newRoomComponentsT.parent = startingT;
			newRoomComponentsT.localPosition = Vector3.zero;
			iInformations.AssignRoomComponents(newRoomComponentsT);

		tRooms[startingI, startingJ] = iInformations;

		DoorSpawner iSpawner = startingT.GetComponent<DoorSpawner>();
		FixDoors(dungeon, startingI, startingJ, iInformations, iSpawner);

		Point startingOffset = startingRoom.position;
		List<RoomInformations> spawnedRoomsNonStarting = new List<RoomInformations>(); //this and all it comes with is VERY unoptimized and only done due to time constraint reasons

		for(int i = 0; i < generator.fixedDungeonRows; i++) {
			for(int j = 0; j < generator.fixedDungeonColumns; j++) {
				Room thisRoom = dungeon[i, j];
				if(startingRoom != thisRoom && thisRoom.roomType != RoomType.Empty) {
					Point thisPoint = thisRoom.position;
					Vector3 newRoomPosition = new Vector3(	(thisPoint.x - startingOffset.x) * Utilities.roomSize.x,
					                                    	-40,
					                                      	(startingOffset.y - thisPoint.y) * Utilities.roomSize.z);
					GameObject newRoom = (GameObject)Instantiate(roomPrefab, newRoomPosition, Quaternion.identity);
					Transform newRoomT = newRoom.transform;
					newRoomT.parent = myTransform;

					RoomInformations rInformations = tRooms[i, j] = newRoom.GetComponent<RoomInformations>();

					spawnedRoomsNonStarting.Add(rInformations);

					GameObject[] listToUtilize = new GameObject[0];
					if(thisRoom.roomType == RoomType.Hub || thisRoom.roomType == RoomType.Corridor) {
						listToUtilize = daMofoList.normalRooms;
					}

					if(listToUtilize.Length > 0) {
						int randomizer = random.constructionRandom.Next(listToUtilize.Length);
						newRoomComponents = (GameObject)Instantiate(listToUtilize[randomizer]);
						newRoomComponentsT = newRoomComponents.transform;

						newRoomComponentsT.parent = newRoomT;
						newRoomComponentsT.localPosition = Vector3.zero;
						rInformations.AssignRoomComponents(newRoomComponentsT);
					}

					DoorSpawner dSpawner = newRoom.GetComponent<DoorSpawner>();
					FixDoors(dungeon, i, j, rInformations, dSpawner);
     			}
			}
		}

		int[] indexes = new int[4];
		for(int k = 0; k < indexes.Length; k++)
			indexes[k] = -1;

		int filled = 0;
		do { 
			int randomized = random.constructionRandom.Next(spawnedRoomsNonStarting.Count);

			bool found = false;
			for(int k = 0; k < indexes.Length; k++) {
				if(randomized == indexes[k])
					found = true;
			}

			if(!found) {
				indexes[filled] = randomized;
				filled++;
			}
		} while(filled < 4);

		List<int> usedToGet = new List<int>();
		usedToGet.Add(0);	usedToGet.Add(1);	usedToGet.Add(2);	usedToGet.Add(3);

		for(int k = 0; k < indexes.Length; k++) {
			RoomInformations ri = spawnedRoomsNonStarting[indexes[k]];
			Transform center = ri.transform.FindChild("Center");
			Transform objects = ri.roomComponents.FindChild("Objects");

			int rndO = random.constructionRandom.Next(usedToGet.Count);
			int indexToSpawn = usedToGet[rndO];
			usedToGet.RemoveAt(rndO);

			GameObject objectGoal = (GameObject)Instantiate(gObjects.goalObjects[indexToSpawn]);
			Transform objectGoalT = objectGoal.transform;

			objectGoalT.position = center.position;
			objectGoalT.parent = objects;
		}

		for(int i = 0; i < generator.fixedDungeonRows; i++) {
			for(int j = 0; j < generator.fixedDungeonColumns; j++) {
				//I had to hack this, i don't know why. Jam stuff.
				if(tRooms[i, j] != null) {
					if(i != 0)
						tRooms[i, j].leftRoom = tRooms[i - 1, j];
					if(i != generator.fixedDungeonRows - 1)
						tRooms[i, j].rightRoom = tRooms[i + 1, j];

					if(j != 0)
						tRooms[i, j].backRoom = tRooms[i, j - 1];
					if(j != generator.fixedDungeonColumns - 1)
						tRooms[i, j].frontRoom = tRooms[i, j + 1];
				}
			}
		}
	}

	private void FixDoors(Room[,] dungeon, int i, int j, RoomInformations rInformations, DoorSpawner dSpawner) {
		if(i != 0 && dungeon[i - 1, j].roomType != RoomType.Empty)	
			rInformations.leftDoor = dSpawner.SpawnLeftDoor();

		if(i != dungeon.GetLength(0) - 1 && dungeon[i + 1, j].roomType != RoomType.Empty)
			rInformations.rightDoor = dSpawner.SpawnRightDoor();
		
		if(j != 0 && dungeon[i, j - 1].roomType != RoomType.Empty)
			rInformations.backDoor = dSpawner.SpawnBackDoor();

		if(j != dungeon.GetLength(1) - 1 && dungeon[i, j + 1].roomType != RoomType.Empty)
			rInformations.frontDoor = dSpawner.SpawnFrontDoor();
	}
}
