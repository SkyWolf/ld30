﻿using UnityEngine;
using System.Collections;

public class TentacleMonsterAnimationReceiver : MonoBehaviour {
	public TentacleMonster theMonstah;

	public void StartToWalk() {
		theMonstah.ActivateMovement();
	}
}
