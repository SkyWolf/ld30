﻿using UnityEngine;
using System.Collections;

public class GUIController : MonoBehaviour {
	public GameObject endScreenBG, endScreenLOGO;
	public GameObject gameOverBG, gameOverLOGO;
	public GameObject howTo;

	public LifeCounterUI lifeCounter;

	public InputManager iManager;

	private bool isStillStarting = true;
	public void HideHowTo() {
		isStillStarting = false;

		howTo.SetActive(false);
		iManager.enabled = true;
	}
	
	private bool isGameOver = false;
	public void ShowGameOver() {
		isGameOver = true;

		gameOverBG.SetActive(true);
		gameOverLOGO.SetActive(true);
	}

	private bool hasEnded = false;
	public void ShowEndScreen() {
		hasEnded = true;

		endScreenBG.SetActive(true);
		endScreenLOGO.SetActive(true);
	}

	void Update() {
		if(isGameOver && Input.GetKeyDown(KeyCode.R))
			Application.LoadLevel(Application.loadedLevel);
		if(isStillStarting && Input.GetKeyDown(KeyCode.Space))
			HideHowTo();
		if(hasEnded) {
			if(Input.GetKeyDown(KeyCode.R))
				Application.LoadLevel(Application.loadedLevel);
			else if(Input.GetKeyDown(KeyCode.Escape))
				Application.Quit();
		}
	}
}
