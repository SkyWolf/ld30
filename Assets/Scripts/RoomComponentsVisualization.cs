﻿using UnityEngine;
using System.Collections;

public class RoomComponentsVisualization : MonoBehaviour {
	private Transform myTransform;

	public Vector3 center;
	public Vector3 size;

	public Color color;

	void Awake() {
		myTransform = transform;
	}

	void OnDrawGizmos() {
		Gizmos.color = color;
		Gizmos.DrawCube(transform.position + center, size);
	}
}
