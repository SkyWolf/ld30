﻿using UnityEngine;
using System.Collections;

public class AntaOpening : MonoBehaviour {
	public enum AntaStatus {
		Closed,
		Closing,
		Opened,
		Opening
	}
	public AntaStatus actualStatus { get; private set; }
	public AntaStatus initialState;

	public Transform hinge;
	public float openedAmount, closedAmount;

	public float timeOfOpening;
	private float timeCollector;

	private Collider myCollider;
	public Collider antaCollider;

	void Start() {
		int i = (int)initialState;
		actualStatus = (AntaStatus)(i - (i%2));

		myCollider = collider;

		if(actualStatus == AntaStatus.Closed)
			CloseInstantly();
		else
			OpenInstantly();
	}

	public void EnableCollider() {
		myCollider.enabled = true;
	}

	public void OpenInstantly() {
		hinge.localRotation = Quaternion.Euler(0, openedAmount, 0);
		EnableCollider();
		antaCollider.enabled = true;
	}

	public void DisableCollider() {
		myCollider.enabled = false;
	}

	public void CloseInstantly() {
		hinge.localRotation = Quaternion.Euler(0, closedAmount, 0);
		DisableCollider();
		antaCollider.enabled = false;
	}

	void Update() {
		switch(actualStatus) {
			case AntaStatus.Closing:
				timeCollector += Time.deltaTime;	
				hinge.localRotation = Quaternion.Euler(0, Mathf.SmoothStep(openedAmount, closedAmount, timeCollector / timeOfOpening), 0);
				
				if(timeCollector >= timeOfOpening) {
					actualStatus = AntaStatus.Closed;
					CloseInstantly();
				}
				break;
			case AntaStatus.Opening:
				timeCollector += Time.deltaTime;  
				hinge.localRotation = Quaternion.Euler(0, Mathf.SmoothStep(closedAmount, openedAmount, timeCollector / timeOfOpening), 0);
			    
				if(timeCollector >= timeOfOpening) {
					actualStatus = AntaStatus.Opened;
					OpenInstantly();
				}
				break;
		}
	}


	public void Open() {
		if(actualStatus != AntaStatus.Closed)
			return;

		timeCollector = 0;
		actualStatus = AntaStatus.Opening;
	}

	public void Close() {
		if(actualStatus != AntaStatus.Opened)
			return;

		timeCollector = 0;
		actualStatus = AntaStatus.Closing;
	}
}