﻿using UnityEngine;
using System.Collections.Generic;

public class SpawnMonsters : MonoBehaviour
{
	public GameObject[] allowedMonsters;
	private List<GameObject> strippedAllowedMonsters;

	private GlobalRandomRefs random;
	private List<SpawnPoint> bigSpawnPoints;
	private List<SpawnPoint> mediumSpawnPoints;
	private List<SpawnPoint> smallSpawnPoints;

	public int howManyToSpawn;
	private Transform myTransform;

	private Transform monsterContainer;

	void Awake() {
		random = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalRandomRefs>();
		myTransform = transform;

		Transform theParent = myTransform.parent;
		if(theParent != null)
			monsterContainer = theParent.FindChild("Monsters");

		strippedAllowedMonsters = new List<GameObject>();
		bigSpawnPoints = new List<SpawnPoint>();
		mediumSpawnPoints = new List<SpawnPoint>();
		smallSpawnPoints = new List<SpawnPoint>();

		Initialize();
	}

	void Initialize() {
		int numSpawns = transform.childCount;

		bigSpawnPoints.Clear();
		mediumSpawnPoints.Clear();
		smallSpawnPoints.Clear();
		for(int i = 0; i < numSpawns; i++) {
			SpawnPoint daPoint = myTransform.GetChild(i).GetComponent<SpawnPoint>();
			if((int)daPoint.acceptedEnemies == (int)Utilities.MonsterSize.Big) {
				bigSpawnPoints.Add(daPoint);
				mediumSpawnPoints.Add(daPoint);
				smallSpawnPoints.Add(daPoint);
			}
			else if((int)daPoint.acceptedEnemies == (int)Utilities.MonsterSize.Big) {
				mediumSpawnPoints.Add(daPoint);
				smallSpawnPoints.Add(daPoint);
			}
			else {
				smallSpawnPoints.Add(daPoint);
			}
		}

		int bigPossible = bigSpawnPoints.Count;
		int mediumPossible = bigPossible + mediumSpawnPoints.Count;
		int smallPossible = mediumPossible + smallSpawnPoints.Count;

		strippedAllowedMonsters.Clear();
		for(int j = 0; j < allowedMonsters.Length; j++) {
			MonsterDescription md = allowedMonsters[j].GetComponent<MonsterDescription>();
			if(md != null) {
				if(md.size == Utilities.MonsterSize.Big && bigPossible > 0)
					strippedAllowedMonsters.Add(allowedMonsters[j]);
				else if(md.size == Utilities.MonsterSize.Medium && mediumPossible > 0)
					strippedAllowedMonsters.Add(allowedMonsters[j]);
				else if(md.size == Utilities.MonsterSize.Little && smallPossible > 0)
					strippedAllowedMonsters.Add(allowedMonsters[j]);
			}
		}

		if(strippedAllowedMonsters.Count == 0)
			return;

		int realSpawns = Mathf.Min(howManyToSpawn, numSpawns);

		for(int i = 0; i < realSpawns && strippedAllowedMonsters.Count != 0; i++) {
			int randomized = random.constructionRandom.Next(strippedAllowedMonsters.Count);
			GameObject monster = strippedAllowedMonsters[randomized];

			SpawnPoint randSP;
			MonsterDescription md = monster.GetComponent<MonsterDescription>();
			if(md.size == Utilities.MonsterSize.Big) {
				randomized = random.constructionRandom.Next(bigSpawnPoints.Count);
				randSP = bigSpawnPoints[randomized];

				bigSpawnPoints.RemoveAt(randomized);
				mediumSpawnPoints.Remove(randSP);
				smallSpawnPoints.Remove(randSP);
			}
			else if(md.size == Utilities.MonsterSize.Medium) {
				randomized = random.constructionRandom.Next(mediumSpawnPoints.Count);
				randSP = mediumSpawnPoints[randomized];

				mediumSpawnPoints.RemoveAt(randomized);
				smallSpawnPoints.Remove(randSP);
			}
			else {
				randomized = random.constructionRandom.Next(smallSpawnPoints.Count);
				randSP = smallSpawnPoints[randomized];

				smallSpawnPoints.RemoveAt(randomized);
			}

			//Big mess pile of shite >_> non-optimized at all
			if(bigSpawnPoints.Count == 0) {
				RemoveAllBig(strippedAllowedMonsters);
				if(mediumSpawnPoints.Count == 0) {
					RemoveAllMedium(strippedAllowedMonsters);
					if(smallSpawnPoints.Count == 0) {
						RemoveAllSmall(strippedAllowedMonsters);
					}
				}
			}

			GameObject monstah = (GameObject)Instantiate(monster, randSP.myTransform.position, Quaternion.identity);
			monstah.transform.parent = monsterContainer;
		}
	}

	private void RemoveAllBig(List<GameObject> monsters) {
		int count = monsters.Count;
		for(int i = 0; i < count; ) {
			if(monsters[i].GetComponent<MonsterDescription>().size == Utilities.MonsterSize.Big) {
				monsters.RemoveAt(i);
				count = monsters.Count;
			}
			else
				i++;
		}
	}

	private void RemoveAllMedium(List<GameObject> monsters) {
		int count = monsters.Count;
		for(int i = 0; i < count; ) {
			if(monsters[i].GetComponent<MonsterDescription>().size == Utilities.MonsterSize.Medium) {
				monsters.RemoveAt(i);
				count = monsters.Count;
			}
			else
				i++;
		}
	}

	private void RemoveAllSmall(List<GameObject> monsters) {
		int count = monsters.Count;
		for(int i = 0; i < count; ) {
			if(monsters[i].GetComponent<MonsterDescription>().size == Utilities.MonsterSize.Little) {
				monsters.RemoveAt(i);
				count = monsters.Count;
			}
			else
				i++;
		}
	}
}