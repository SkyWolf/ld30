﻿using UnityEngine;
using System.Collections.Generic;

public class NightmareController : MonoBehaviour {
	public Material dayAtlas, nightAtlas;
	public Material dayFloor, nightFloor;

	public MeshRenderer behindWall, leftWall, rightWall;
	public MeshRenderer floor;

	private Transform myTransform;
	private RoomInformations info;
	private Transform monstersList;

	private List<MeshRenderer> doorRenderers;
	private List<AntaOpening> doorOpenings;

	private RoomMovement movement;
	private LightsOff terrorController;

	public bool playerIsInMeKappa { get; private set; }

	void Start() {
		myTransform = transform;
		info = GetComponent<RoomInformations>();

		doorRenderers = new List<MeshRenderer>();
		doorOpenings = new List<AntaOpening>();

		movement = GetComponent<RoomMovement>();

		foreach(Transform child in myTransform) {
			if(child.tag.Equals("Door")) {
				MeshRenderer[] dMR = child.GetComponentsInChildren<MeshRenderer>();
				for(int i = 0; i < dMR.Length; i++) {
					doorRenderers.Add(dMR[i]);
				}

				doorOpenings.Add(child.GetComponent<AntaOpening>());
			}
		}
	}

	public void PlayerHasEntered() {
		playerIsInMeKappa = true;
	}

	private void ActivateMonsters() {
		if(!playerIsInMeKappa)
			return;

		if(!monstersList)
			monstersList = info.roomComponents.FindChild("Monsters");

		foreach(Transform monster in monstersList) {
			monster.gameObject.SetActive(true);
		}
	}

	public void PlayerHasExited() {
		playerIsInMeKappa = false;
	}

	private void DeactivateMonsters() {
		if(!playerIsInMeKappa)
			return;

		if(!monstersList)
			monstersList = info.roomComponents.FindChild("Monsters");

		foreach(Transform monster in monstersList) {
			monster.gameObject.SetActive(false);
		}
	}

	public void GoNightmare() {
		behindWall.sharedMaterial = nightAtlas;
		leftWall.sharedMaterial = nightAtlas;
		rightWall.sharedMaterial = nightAtlas;

		Material[] floorMaterials = floor.sharedMaterials;
		floorMaterials[0] = nightFloor;
		floorMaterials[1] = nightAtlas;
		floor.sharedMaterials = floorMaterials;

		for(int i = 0; i < doorRenderers.Count; i++)
			doorRenderers[i].sharedMaterial = nightAtlas;

		ActivateMonsters();
	}

	public void DisableAllDoorsColliders() {
		for(int i = 0; i < doorOpenings.Count; i++)
			doorOpenings[i].DisableCollider();
	}

	public void CloseAllDoors() {
		for(int i = 0; i < doorOpenings.Count; i++)
			doorOpenings[i].Close();
	}

	public void GoDay() {
		behindWall.sharedMaterial = dayAtlas;
		leftWall.sharedMaterial = dayAtlas;
		rightWall.sharedMaterial = dayAtlas;
		
		Material[] floorMaterials = floor.sharedMaterials;
		floorMaterials[0] = dayFloor;
		floorMaterials[1] = dayAtlas;
		floor.sharedMaterials = floorMaterials;

		for(int i = 0; i < doorRenderers.Count; i++)
			doorRenderers[i].sharedMaterial = dayAtlas;

		DeactivateMonsters();
	}

	public void OpenAllDoors() {
		for(int i = 0; i < doorOpenings.Count; i++)
			doorOpenings[i].Open();
	}
}