﻿using UnityEngine;
using System.Collections;

public class ObjectMagnet : MonoBehaviour {
	[HideInInspector]
	public ObjectSpotController stayTogether;
	private Transform myTransform;
	private Rigidbody myRigidbody;

	public float metersPerSecond;

	void Awake() {
		myTransform = transform;
		myRigidbody = rigidbody;
	}

	public bool hasBeenThrown;

	private bool isSnapped;
	private Transform lastActiveSnap;

	private IEnumerator CheckMovement() {
		yield return new WaitForSeconds(0.1f);
		while(true) {
			if(hasBeenThrown) {
				if(myRigidbody.velocity.sqrMagnitude < 0.1f)
					hasBeenThrown = false;
			}
			yield return new WaitForFixedUpdate();
		}
	}

	void Update() {
		if(stayTogether != null) {
			Vector3 direction = stayTogether.activeObjectSpot.position - myTransform.position;
			if(isSnapped && lastActiveSnap != stayTogether.activeObjectSpot) {
				lastActiveSnap = myTransform.parent = stayTogether.activeObjectSpot;
			}
			else {
				if(direction.sqrMagnitude < 0.01f) {
					lastActiveSnap = myTransform.parent = stayTogether.activeObjectSpot;
				}
				else {
					direction.Normalize();
					myTransform.position += direction * metersPerSecond * Time.deltaTime;
				}
			}
		}
	}
}
