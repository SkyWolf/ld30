﻿using UnityEngine;
using System.Collections;

public class TentacleMonster : Monster {
	private GlobalRandomRefs random;

	public override void Configure() {
		random = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalRandomRefs>();
	}

	private float cumulativeThinkingTime;
	public override void EnterThink() {
		base.EnterThink();
		cumulativeThinkingTime = 0;

		myAnimator.SetBool("isMoving", false);
	}

	public float howMuchShouldThink;
	public override void Think(float dTime) {
		cumulativeThinkingTime += dTime;
		if(cumulativeThinkingTime >= howMuchShouldThink) {
			if(random.behaviourRandom.NextDouble() > 0.4) { //check se abbastanza vicino al personaggio, se si attaccalo
				ChangeState(Behaviour.Attacking);
			}
			else {
				ChangeState(Behaviour.MovingTowards);
			}
		}
	}
	
	int timesMoved;
	public override void EnterMove() {
		base.EnterMove();
		timesMoved = 0;

		myAnimator.SetBool("isMoving", true);
	}
	
	private float speed;
	public override void Move(float dTime) {
		if(isMoveable) {
			speed = Mathf.Min(maxSpeed, speed + (acceleration * dTime));

			Vector3 direction = movingTowards.position - myRigidbody.position;
			direction.Normalize();

			myRigidbody.position += direction * maxSpeed * dTime;
		}
	}

	bool isMoveable = false;
	public void ActivateMovement() {
		if(behave != Behaviour.MovingTowards)	return;

		//Da qui nei prossimi 0.4s mi posso muovere
		timesMoved++;
		if(timesMoved == 3)
			ChangeState(Behaviour.Thinking);

		isMoveable = true;
		StartCoroutine(StopMoveable());
		speed = 0;
	}

	private IEnumerator StopMoveable() {
		yield return new WaitForSeconds(0.7f);
		isMoveable = false;
	}
	
	private Vector3 throwYourselfTowards;
	private float cumulativeAttackingTime = 0;
	private float cumulativeChargeAmount = 0;
	public override void EnterAttack() {
		cumulativeAttackingTime = 0;
		cumulativeChargeAmount = 0;
		throwYourselfTowards = thePlayer.position;
		setTriggerOnce = false;
		setDashEndOnce = false;
	}

	public float CHHHAAAAARGEspeed;
	public float secondsToAttack;
	public float maxChargeAmount;
	public bool setTriggerOnce = false;
	public bool setDashEndOnce = false;
	public override void Attack(float dTime) {
		cumulativeAttackingTime += dTime;
		if(cumulativeAttackingTime >= secondsToAttack) {
			if(!setTriggerOnce) {
				myAnimator.SetTrigger("attack");
				currentDamage = attackingDamage;
				setTriggerOnce = true;
			}

			Vector3 direction = (throwYourselfTowards - myRigidbody.position);
			direction.Normalize();

			Vector3 movement = direction * CHHHAAAAARGEspeed * dTime;
			myRigidbody.position += movement;

			cumulativeChargeAmount += movement.magnitude;

			if(cumulativeChargeAmount >= maxChargeAmount) {
				ChangeState(Behaviour.Thinking);
				if(!setDashEndOnce) {
					myAnimator.SetTrigger("attackEnd");
					setDashEndOnce = true;
				}
			}
		}
	}

	public override void EnterKnockback() {
		base.EnterKnockback();
		myAnimator.SetTrigger("beenHit");
	}

	public float knockbackDuration;
	private float cumulativeKnockbackTime;
	public override void Knockback(float dTime) {
		cumulativeKnockbackTime += dTime;
		if(cumulativeKnockbackTime >= knockbackDuration) {
			ChangeState(Behaviour.Thinking);
		}
	}
}
