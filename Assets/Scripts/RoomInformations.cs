﻿using UnityEngine;
using System.Collections;

public class RoomInformations : MonoBehaviour {
	public Room roomDescription;
	public Transform myTransform;
	public Transform roomComponents { get; private set; }
	private Transform monstersContainer;

	public void AssignRoomComponents(Transform _roomComponents) {
		if(!myTransform)
			myTransform = transform;
		
		roomComponents = _roomComponents;
		roomComponents.transform.parent = myTransform;
		monstersContainer = roomComponents.FindChild("Monsters");
	}

	public bool AreThereAnyMonsters() {
		return monstersContainer.childCount > 0;
	}

	private Transform _backDoor, _leftDoor, _frontDoor, _rightDoor;
	public Transform[] orderedDoors = new Transform[4];
	
	public Transform backDoor {
		get { 	return _backDoor;				}
		set {	_backDoor = value;	
				orderedDoors[0] = _backDoor; 	}
	}
	
	public Transform leftDoor {
		get { 	return _leftDoor;				}
		set {	_leftDoor = value;	
				orderedDoors[1] = _leftDoor; 	}
	}
	
	public Transform frontDoor {
		get { 	return _frontDoor;				}
		set {	_frontDoor = value;	
				orderedDoors[2] = _frontDoor; 	}
	}
	
	public Transform rightDoor {
		get { 	return _rightDoor;				}
		set {	_rightDoor = value;	
				orderedDoors[3] = _rightDoor; 	}
	}

	private RoomInformations _backRoom, _leftRoom, _frontRoom, _rightRoom;
	public RoomInformations[] orderedRooms = new RoomInformations[4];

	public RoomInformations backRoom {
		get { 	return _backRoom;				}
		set {	_backRoom = value;	
				orderedRooms[0] = _backRoom; 	}
	}

	public RoomInformations leftRoom {
		get { 	return _leftRoom;				}
		set {	_leftRoom = value;	
				orderedRooms[1] = _leftRoom; 	}
	}

	public RoomInformations frontRoom {
		get { 	return _frontRoom;				}
		set {	_frontRoom = value;	
				orderedRooms[2] = _frontRoom; 	}
	}

	public RoomInformations rightRoom {
		get { 	return _rightRoom;				}
		set {	_rightRoom = value;	
				orderedRooms[3] = _rightRoom; 	}
	}
}