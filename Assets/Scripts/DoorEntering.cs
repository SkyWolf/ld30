﻿using UnityEngine;
using System.Collections;

public class DoorEntering : MonoBehaviour {
	public Teleporter tp;

	private Rigidbody myRigidbody;
	private Collider ignoreUntilExit;

	private PlayerMovement pMovement;

	void Awake() {
		myRigidbody = rigidbody;
		pMovement = GetComponent<PlayerMovement>();
	}

	void OnTriggerEnter(Collider col) {
		if(col.tag.Equals("Door") && !col.Equals(ignoreUntilExit)) {
			DoorInformations dInfo = col.GetComponent<DoorInformations>();

			RoomInformations goingFrom = dInfo.owner;
			RoomInformations goingTo = goingFrom.orderedRooms[(int)dInfo.whichDoor];	//TODO: check why sometimes null reference here
			pMovement.whichRoomIAmIn = goingTo;

			BoxCollider spawningInto = (BoxCollider)goingTo.orderedDoors[(int)(dInfo.whichDoor + 2) % 4].collider;
			ignoreUntilExit = spawningInto;

			tp.Teleport(myRigidbody, goingFrom, goingTo, spawningInto, dInfo);
		}
	}

	void OnTriggerExit(Collider col) {
		if(col.tag.Equals("Door") && col.Equals(ignoreUntilExit)) {
			ignoreUntilExit = null;
		}
	}
}