﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {
	public enum Peripherals {
		Keyboard,
		Joypad
	}

	public Peripherals currentPeripheral = Peripherals.Keyboard;
	
	public Vector3 lastDirection { get; private set; }
	public bool firedThisFrame { get; private set; }

	void Update() {
		Vector3 buildingDirection = Vector3.zero;
		firedThisFrame = false;

		switch(currentPeripheral) {
			case Peripherals.Keyboard:
				if(Input.GetKey(KeyCode.W)) //TODO: change keycode to a dynamic one
					buildingDirection.z += 1;
				if(Input.GetKey(KeyCode.A)) //TODO: change keycode to a dynamic one
					buildingDirection.x -= 1;
				if(Input.GetKey(KeyCode.S)) //TODO: change keycode to a dynamic one
					buildingDirection.z -= 1;
				if(Input.GetKey(KeyCode.D)) //TODO: change keycode to a dynamic one
					buildingDirection.x += 1;

				if(Input.GetKeyDown(KeyCode.Space))
					firedThisFrame = true;

				break;
			case Peripherals.Joypad:
				buildingDirection.Set(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

				break;

		}

		lastDirection = GetCloserAngle(buildingDirection);
		lastDirection *= Mathf.Min(1, buildingDirection.magnitude);
	}

	private Vector3 GetCloserAngle(Vector3 building) {
		if(building.Equals(Vector3.zero))
			return Vector3.zero;

		float angle = Vector3.Angle(building, Vector3.forward);
		if(Vector3.Angle(building, Vector3.right) > 90)
			angle = 360 - angle;
		angle += 45f / 2;
		angle %= 360;

		int index = (int)(angle / 45);
		return Utilities.eightDirections[index];
	}
}