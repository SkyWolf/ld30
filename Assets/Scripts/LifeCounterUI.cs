﻿using UnityEngine;
using System.Collections;

public class LifeCounterUI : MonoBehaviour {
	public HearthContainer[] theHearts;

	public void RepresentHealth( int amount ) {
		int amtwo = amount / 2;
		int res = amount % 2;

		int i;
		for(i = 0; i < amtwo; i++)
			theHearts[i].ShowFull();
		if(res == 1) {
			theHearts[i].ShowHalf();
			i++;
		}
		for(; i < theHearts.Length; i++) {
			theHearts[i].ShowEmpty();
		}
	}
}
