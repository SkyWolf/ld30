﻿using UnityEngine;
using System.Collections.Generic;

public class Point {
	public int x;
	public int y;

	public Point(int _x, int _y) {
		x = _x;
		y = _y;
	}

	public override bool Equals (object obj)
	{
		if(!(obj is Point))
			return false;

		Point objP = (Point)obj;
		if(objP.x != x || objP.y != y)
			return false;

		return true;
	}
}

public enum RoomType {
	Empty,
	Hub,
	Corridor,
	Starting
}
public class Room {
	public RoomType roomType;
	public Point position;

	public Room(Point _position) {
		position = _position;
	}
}

public class Hub : Room {
	public List<Hub> connectedTo;

	public Hub(Point _position) : base(_position) {
		connectedTo = new List<Hub>();
	}
}

public class StartingRoom : Hub {
	public StartingRoom(Point _position) : base(_position) { }
}