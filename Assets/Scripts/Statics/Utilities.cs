﻿using UnityEngine;
using System.Collections;

public static class Utilities {
	public static Vector3[] eightDirections = {		new Vector3(0, 0, 1), new Vector3(0.7071068f, 0, 0.7071068f),
													new Vector3(1, 0, 0), new Vector3(0.7071068f, 0, -0.7071068f),
													new Vector3(0, 0, -1), new Vector3(-0.7071068f, 0, -0.7071068f),
													new Vector3(-1, 0, 0), new Vector3(-0.7071068f, 0, 0.7071068f)
												};

	public static Vector3 roomSize = new Vector3(11.4f, 0, 7.54f);

	public enum MonsterSize {
		Little,
		Medium,
		Big
	}
}