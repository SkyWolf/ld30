﻿using UnityEngine;
using System.Collections;

public class Monster : MonoBehaviour {
	public enum Behaviour {
		Thinking,
		MovingTowards,
		Attacking,
		Knockback,
		Nil
	}

	public Behaviour behave { get; private set; }
	private Behaviour previousBehave = Behaviour.Nil;
	public Transform movingTowards { get; private set; }

	[HideInInspector]
	public Animator myAnimator;

	public Rigidbody myRigidbody { get; private set; }
	public Transform thePlayer { get; private set; }

	public float maxSpeed = 3;
	public float acceleration = 2;

	[HideInInspector]
	public int currentDamage;

	public int idleDamage, movingDamage, attackingDamage, knockoutDamage;

	void Awake() {
		thePlayer = GameObject.FindGameObjectWithTag("Player").transform;
		behave = Behaviour.Thinking;

		myRigidbody = rigidbody;
		movingTowards = thePlayer;
		myAnimator = GetComponentsInChildren<Animator>()[0];

		currentDamage = 1;

		Configure();
	}

	void Update() {
		switch(behave) {
			case Behaviour.Thinking:
				if(behave != previousBehave) {
					EnterThink();
					previousBehave = behave;
				}
				Think(Time.deltaTime);
				break;
			case Behaviour.MovingTowards:
				if(behave != previousBehave) {
					EnterMove();
					previousBehave = behave;
				}
				Move(Time.deltaTime);
				break;

			case Behaviour.Attacking:
				if(behave != previousBehave) {
					EnterAttack();
					previousBehave = behave;
				}
				Attack(Time.deltaTime);
				break;

			case Behaviour.Knockback:
				if(behave != previousBehave) {
					EnterKnockback();
					previousBehave = behave;
				}
				Knockback(Time.deltaTime);
				break;
		}
	}

	public virtual void Configure() { }

	public void ChangeState(Behaviour newState) {
		if(newState == Behaviour.Nil)
			return;
		behave = newState;
	}

	public virtual void Think(float dTime) { }
	public virtual void Move(float dTime) { }
	public virtual void Attack(float dTime) { }
	public virtual void Knockback(float dTime) { }

	public virtual void EnterThink() { currentDamage = idleDamage; }
	public virtual void EnterMove() { currentDamage = movingDamage; }
	public virtual void EnterAttack() { currentDamage = attackingDamage; }
	public virtual void EnterKnockback() { currentDamage = knockoutDamage; }

	void OnCollisionEnter(Collision col) {
		Collider collider = col.collider;
		GameObject go = collider.gameObject;
		
		int layer = go.layer;
		if(layer == LayerMask.NameToLayer("Object")) {
			Vector3 relVelocity = col.relativeVelocity;

			relVelocity.y = 0;

			myRigidbody.AddForce(relVelocity * 20, ForceMode.Impulse);
			ObjectMagnet omg = go.GetComponent<ObjectMagnet>();
			
			if(omg.hasBeenThrown) {
				ChangeState(Behaviour.Knockback);
				omg.hasBeenThrown = false;
			}
		}
		else if(layer == LayerMask.NameToLayer("Wall") && behave == Behaviour.Attacking) {
			ChangeState(Behaviour.Knockback);
		}
	}
}
