﻿using UnityEngine;
using System.Collections;

public class DoorSpawner : MonoBehaviour {
	public GameObject doorPrefab;

	public Transform leftWallPivot;
	public Transform rightWallPivot;
	public Transform backWallPivot;
	public Transform frontWallPivot;

	private Transform myTransform;

	public Transform SpawnBackDoor() {
		if(!myTransform)
			myTransform = transform;

		GameObject backDoor = (GameObject)Instantiate(doorPrefab, backWallPivot.position, Quaternion.identity);
		Transform backDoorT = backDoor.transform;

		DoorInformations theDoor = backDoor.GetComponent<DoorInformations>();
		theDoor.owner = myTransform.GetComponent<RoomInformations>();
		theDoor.whichDoor = DoorInformations.DoorSide.Up;

		backDoorT.parent = myTransform;

		return backDoorT;
	}
	public Transform SpawnLeftDoor() {
		if(!myTransform)
			myTransform = transform;

		GameObject leftDoor = (GameObject)Instantiate(doorPrefab, leftWallPivot.position, Quaternion.Euler(0, -90, 0));
		Transform leftDoorT = leftDoor.transform;

		DoorInformations theDoor = leftDoor.GetComponent<DoorInformations>();
		theDoor.owner = myTransform.GetComponent<RoomInformations>();
		theDoor.whichDoor = DoorInformations.DoorSide.Left;
		
		leftDoorT.parent = myTransform;

		return leftDoorT;
	}
	public Transform SpawnFrontDoor() {
		if(!myTransform)
			myTransform = transform;

		GameObject frontDoor = (GameObject)Instantiate(doorPrefab, frontWallPivot.position, Quaternion.Euler(0, 180, 0));
		Transform frontDoorT = frontDoor.transform;

		DoorInformations theDoor = frontDoor.GetComponent<DoorInformations>();
		theDoor.owner = myTransform.GetComponent<RoomInformations>();
		theDoor.whichDoor = DoorInformations.DoorSide.Down;
		
		frontDoorT.parent = myTransform;

		return frontDoorT;
	}
	public Transform SpawnRightDoor() {
		if(!myTransform)
			myTransform = transform;
		
		GameObject rightDoor = (GameObject)Instantiate(doorPrefab, rightWallPivot.position, Quaternion.Euler(0, 90, 0));
		Transform rightDoorT = rightDoor.transform;
		
		DoorInformations theDoor = rightDoor.GetComponent<DoorInformations>();
		theDoor.owner = myTransform.GetComponent<RoomInformations>();
		theDoor.whichDoor = DoorInformations.DoorSide.Right;
		
		rightDoorT.parent = myTransform;
		
		return rightDoorT;
	}
}