﻿using UnityEngine;
using System.Collections;

public class CameraTransition : MonoBehaviour {
	public enum CameraState {
		Ready,
		Transitioning
	}
	public CameraState actualState { get; private set; }

	private Vector3 transitionFrom, transitionTo;
	private float timeCollector;

	public float transitionInTime;

	private Transform myTransform;

	void Awake() {
		myTransform = transform;
	}

	void Update() {
		if(actualState == CameraState.Transitioning) {
			timeCollector += Time.deltaTime;

			myTransform.position = new Vector3(	Mathf.SmoothStep(transitionFrom.x, transitionTo.x, timeCollector / transitionInTime),
		                                   		myTransform.position.y,
			                                   	Mathf.SmoothStep(transitionFrom.z, transitionTo.z, timeCollector / transitionInTime)	);

			if(timeCollector >= transitionInTime)
				actualState = CameraState.Ready;
		}
	}

	public void GoDirection(DoorInformations.DoorSide side) {
		if(actualState != CameraState.Ready)
			return;

		timeCollector = 0;
		transitionFrom = myTransform.position;
		if(side == DoorInformations.DoorSide.Up) {
			transitionTo = transitionFrom + (Vector3.forward * Utilities.roomSize.z);
		}
		else if(side == DoorInformations.DoorSide.Left) {
			transitionTo = transitionFrom + (Vector3.right * -Utilities.roomSize.x);
		}
		else if(side == DoorInformations.DoorSide.Down) {
			transitionTo = transitionFrom + (Vector3.forward * -Utilities.roomSize.z);
		}
		else if(side == DoorInformations.DoorSide.Right) {
			transitionTo = transitionFrom + (Vector3.right * Utilities.roomSize.x);
		}

		actualState = CameraState.Transitioning;
	}
}
