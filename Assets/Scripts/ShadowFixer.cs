﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ShadowFixer : MonoBehaviour {
	void Awake() {
		renderer.castShadows = true;
		renderer.receiveShadows = false;
	}
}
