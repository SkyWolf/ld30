﻿using UnityEngine;
using System.Collections;

public class WinningCondition : MonoBehaviour {
	private Transform objectsList;
	public GUIController guiC;

	void Start() {
		RoomInformations ri = GetComponent<RoomInformations>();
		objectsList = ri.roomComponents.FindChild("Objects");

		StartCoroutine(CheckWinningCondition());
	}

	private IEnumerator CheckWinningCondition() {
		bool hasWon = false;
		while(!hasWon) {
			int howMany = 0;
			foreach(Transform gObject in objectsList) {
				if(gObject.tag.Equals("GoalObjects"))
					howMany++;
			}
			if(howMany == 4)
				hasWon = true;

			yield return new WaitForSeconds(0.3f);
		}

		Time.timeScale = 0;
		guiC.ShowEndScreen();
	}
}
