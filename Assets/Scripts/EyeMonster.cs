﻿using UnityEngine;
using System.Collections;

public class EyeMonster : Monster {
	private GlobalRandomRefs random;
	public override void Configure() {
		random = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalRandomRefs>();
	}

	private float timeCollector;

	public float howMuchShouldThink;
	public override void Think(float dTime) {
		timeCollector += dTime;

		if(timeCollector >= howMuchShouldThink) {
			if(random.behaviourRandom.NextDouble() < .4f)
				ChangeState(Behaviour.Attacking);
			else
				ChangeState(Behaviour.MovingTowards);
		}
	}

	private float movementCollector;
	public override void Move(float dTime) { 
		float movementAmount = maxSpeed * dTime;
		movementCollector += movementAmount;
		myRigidbody.position += goTo * movementAmount;

		if(movementCollector >= distanceToMoveAt)
			ChangeState(Behaviour.Thinking);
	}

	public float afterHowMuchTimeShouldAttackEnd;
	public float maxAttackingSpeed;

	private float actualAttackingSpeed;
	public override void Attack(float dTime) { 
		timeCollector += dTime;

		Vector3 direction = thePlayer.position - myRigidbody.position;
		direction.Normalize();

		actualAttackingSpeed = Mathf.Min(maxAttackingSpeed, actualAttackingSpeed + (acceleration * dTime));
		myRigidbody.position += direction * actualAttackingSpeed * dTime;

		if(timeCollector >= afterHowMuchTimeShouldAttackEnd)
			ChangeState(Behaviour.Thinking);
	}

	public float knockOutTime;
	public override void Knockback(float dTime) { 
		timeCollector += dTime;

		if(timeCollector >= knockOutTime)
			ChangeState(Behaviour.Thinking);
	}


	public override void EnterThink() {
		base.EnterThink();
		myAnimator.SetBool("isAttacking", false);

		timeCollector = 0;
	}
	
	public float distanceToMoveAt;

	private Vector3 goTo;
	public override void EnterMove() { 
		base.EnterMove();
		myAnimator.SetBool("isAttacking", false);

		goTo = Quaternion.AngleAxis(random.behaviourRandom.Next(0, 360), Vector3.up) * (Vector3.forward);
		movementCollector = 0;
	}

	public override void EnterAttack() { 
		base.EnterAttack();
		myAnimator.SetBool("isAttacking", true);
		timeCollector = 0;
		actualAttackingSpeed = 0;
	}

	public override void EnterKnockback() { 
		base.EnterKnockback();
		myAnimator.SetTrigger("hit");
	}
}
