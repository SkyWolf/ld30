﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {
	public Utilities.MonsterSize acceptedEnemies;
	public Transform myTransform;

	void OnDrawGizmos() {
		Gizmos.color = Color.magenta;
		Gizmos.DrawSphere(transform.position, 0.25f);
	}
}
