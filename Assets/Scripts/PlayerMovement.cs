﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	public InputManager inputManager;
	public Transform aimCone;
	
	public float maxSpeed;
	public float deceleration;

	public Animator playerAnimator;

	public Vector3 lastDirection { get; private set; }

	private Transform myTransform;
	private Rigidbody myRigidbody;
	private float actualSpeed;

	private Transform objectInHand;
	public float distanceToPickupObjects;

	public ObjectSpotController objectSpotController;
	private RoomInformations _whichRoomIAmIn;
	public RoomInformations whichRoomIAmIn {
		get {
			return _whichRoomIAmIn;
		}
		set {
			_whichRoomIAmIn.GetComponent<NightmareController>().PlayerHasExited();
			_whichRoomIAmIn = value;
			_whichRoomIAmIn.GetComponent<NightmareController>().PlayerHasEntered();
		}
	}

	void Awake() {
		myTransform = transform;
		myRigidbody = rigidbody;

		playerAnimator.SetInteger("animDirection", 0);
		playerAnimator.SetFloat("playerSpeed",0.0f);

		objectInHand = null;
		_whichRoomIAmIn = GameObject.FindGameObjectWithTag("StartingRoom").GetComponent<RoomInformations>();
	}

	void Update () {
		float dTime = Time.deltaTime;

		Vector3 direction = inputManager.lastDirection;
		bool firedThisFrame = inputManager.firedThisFrame;

		if(!direction.Equals(Vector3.zero)) {
			actualSpeed = maxSpeed;
			lastDirection = direction;
		}
		else
			actualSpeed = Mathf.Max(0, actualSpeed - (deceleration * dTime));

		myRigidbody.position += lastDirection * actualSpeed * dTime;
		aimCone.LookAt(myRigidbody.position + lastDirection);

		if(firedThisFrame) {
			if(objectInHand == null)
				SearchAndPickupObject();
			else
				ShootObject();
		}

		UpdateAnimationState();
	}

	private void ShootObject() {
		objectInHand.GetComponent<ObjectMagnet>().stayTogether = null;

		ObjectMagnet omg = objectInHand.GetComponent<ObjectMagnet>();
		omg.hasBeenThrown = true;

		Transform roomComponents = whichRoomIAmIn.roomComponents;
		Transform objectList = roomComponents.FindChild("Objects");

		//TODO: A bit unoptimized
		objectInHand.parent = objectList;
		objectInHand.collider.enabled = true;
		objectInHand.rigidbody.isKinematic = false;

		Vector3 launchDirection = ((lastDirection * 10) + (myTransform.up * 2));
		objectInHand.rigidbody.AddForce(launchDirection, ForceMode.Impulse);

		objectInHand = null;
	}

	private void SearchAndPickupObject() {
		Transform roomComponents = whichRoomIAmIn.roomComponents;

		if(roomComponents == null)	return; 

		Transform objectList = roomComponents.FindChild("Objects");
		float sqrDistance = distanceToPickupObjects * distanceToPickupObjects;

		foreach(Transform child in objectList) {
			if(child.rigidbody) {
				if((child.position - myRigidbody.position).sqrMagnitude < sqrDistance) {
					objectInHand = child;

					child.collider.enabled = false;
					child.rigidbody.isKinematic = true;

					ObjectMagnet magnet = child.GetComponent<ObjectMagnet>();
					magnet.stayTogether = objectSpotController; 

					return;
				}
			}
		}
	}

	private void UpdateAnimationState() {
		/////
		/// 1 = up, 2 = right, 3= down, 4=left
		const int _up = 1 , _right= 2 , _down= 3 , _left= 4;
		int animDir = 0;
		if (lastDirection.z > 0)
			animDir = _up;
		else if (lastDirection.z < 0)
			animDir = _down;
		if (lastDirection.x > 0)
			animDir = _right;
		else if (lastDirection.x < 0)
			animDir = _left;
		if (actualSpeed < 0.01f) {//idle
			lastDirection = -Vector3.forward;
			aimCone.LookAt(myRigidbody.position + lastDirection);

			animDir = 0;
		}

		objectSpotController.ActivateSpotDir(animDir);

		playerAnimator.SetInteger("animDirection", animDir);
		playerAnimator.SetFloat("playerSpeed",actualSpeed);
		//Debug.Log (animDir);
	}
}


