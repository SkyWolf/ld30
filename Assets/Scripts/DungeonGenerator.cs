﻿using UnityEngine;
using System.Collections.Generic;

public class DungeonGenerator : MonoBehaviour {
	public Room[,] dungeon { get; private set; }

	public int dungeonRows, dungeonColumns;

	public int fixedDungeonRows { get; private set; }
	public int fixedDungeonColumns { get; private set; }

	public int dungeonHubs;
	public int maximumLinksPerHub;

	public List<Room> hubs;

	private GameObject global;
	private GlobalRandomRefs random;
	private DungeonDataToReal ddtr;

	private int succesfullyCreated;
	private int totalPossibleRooms;
	
	void Awake() {
		global = GameObject.FindGameObjectWithTag("Global");
		random = global.GetComponent<GlobalRandomRefs>();

		dungeon = new Room[dungeonRows, dungeonColumns];
		hubs = new List<Room>();

		ddtr = GetComponent<DungeonDataToReal>();

		Initialize();
	}

	private void Initialize() {
		fixedDungeonRows = dungeonRows;
		fixedDungeonColumns = dungeonColumns;
		for(int i = 0; i < dungeonRows; i++) {
			for(int j = 0; j < dungeonColumns; j++) {
				dungeon[i, j] = new Room(new Point(i, j));
			}
		}
		hubs.Clear();

		GenerateHubs(dungeonHubs);
		ConnectHubs(maximumLinksPerHub);

		//Dirty hack!!!
		if(succesfullyCreated <= totalPossibleRooms * 0.25f)
			Initialize();
		else
			ddtr.Convert(this);
	}

	private void GenerateHubs(int howMany) {
		totalPossibleRooms = dungeonRows * dungeonColumns;
		howMany = Mathf.Clamp(howMany, 2, Mathf.RoundToInt(totalPossibleRooms * .15f));
		
		succesfullyCreated = 0;

		Room randomized;
		while(succesfullyCreated < howMany) {
			int randomizedRow, randomizedColumn;
			bool hasToRegenerate;
			do {
				hasToRegenerate = false;

				int randomizedRoom = random.constructionRandom.Next(totalPossibleRooms);
				if(dungeonColumns < dungeonRows) {
					randomizedRow = randomizedRoom / dungeonColumns;
					randomizedColumn = randomizedRoom % dungeonColumns;
				} 
				else {
					randomizedColumn = randomizedRoom / dungeonRows;
					randomizedRow = randomizedRoom % dungeonRows;
				}

				dungeon[randomizedRow, randomizedColumn] = new Hub(new Point(randomizedRow, randomizedColumn));
				randomized = dungeon[randomizedRow, randomizedColumn];

				if(	randomized.roomType != RoomType.Empty ||
					randomizedRow != 0 && dungeon[randomizedRow - 1, randomizedColumn].roomType == RoomType.Hub ||
				   	randomizedRow != dungeonRows - 1 && dungeon[randomizedRow + 1, randomizedColumn].roomType == RoomType.Hub ||
				   	randomizedColumn != 0 && dungeon[randomizedRow, randomizedColumn - 1].roomType == RoomType.Hub ||
				   	randomizedColumn != dungeonColumns - 1 && dungeon[randomizedRow, randomizedColumn + 1].roomType == RoomType.Hub)
					hasToRegenerate = true;
			} while(hasToRegenerate);
			
			dungeon[randomizedRow, randomizedColumn].roomType = RoomType.Hub;
			hubs.Add(dungeon[randomizedRow, randomizedColumn]);
			succesfullyCreated++;
        }

		int randomizedStarting = random.constructionRandom.Next(hubs.Count);
		int r = hubs[randomizedStarting].position.x;
		int c = hubs[randomizedStarting].position.y;
		hubs.RemoveAt(randomizedStarting);

		dungeon[r, c] = new StartingRoom(new Point(r, c));
		dungeon[r, c].roomType = RoomType.Starting;
		hubs.Add(dungeon[r, c]);
	}

	private void ConnectHubs(int howMany) {
		howMany = Mathf.Clamp(howMany, 1, hubs.Count - 1);
		List<Hub> nonMeHubs = new List<Hub>();

		for(int i = 0; i < hubs.Count; i++) {
			Hub meHub = (Hub)hubs[i];
			Point meHubP = meHub.position;

			nonMeHubs.Clear();
			for(int j = 0; j < hubs.Count; j++) {
				if(j != i)
					nonMeHubs.Add((Hub)hubs[j]);
			}

			int links = random.constructionRandom.Next(howMany) + 1;
			for(int j = 0; j < links && nonMeHubs.Count > 0;) {
				int randomized = random.constructionRandom.Next(nonMeHubs.Count);
				Hub connectTo = nonMeHubs[randomized];

				if(!connectTo.connectedTo.Contains(meHub)) {
					j++;

					meHub.connectedTo.Add(connectTo);
					Point connectToP = connectTo.position;

					//Connect
					//Decide if we're going horizontal or vertical first
					int horizontalDifference = meHubP.x - connectToP.x;
					int verticalDifference = meHubP.y - connectToP.y;
					int absHDifference = Mathf.Abs(horizontalDifference);
					int absVDifference = Mathf.Abs(verticalDifference);

					//if(horizontalDifference > verticalDifference) {
						int cutPoint = random.constructionRandom.Next(absHDifference);
						int horizontalSign = horizontalDifference != 0 ? horizontalDifference / absHDifference : 0;

						int m;
						for(m = 0; m < cutPoint; m++) {
							if(	dungeon[meHubP.x + (m * -horizontalSign), meHubP.y].roomType == RoomType.Empty) {
								dungeon[meHubP.x + (m * -horizontalSign), meHubP.y].roomType = RoomType.Corridor;
								succesfullyCreated++;
							}
						}

						int verticalSign = verticalDifference != 0 ? verticalDifference / absVDifference : 0;

						for(int n = 0; n < absVDifference; n++) {
							if(	dungeon[meHubP.x + (m * -horizontalSign), meHubP.y + (n * -verticalSign)].roomType == RoomType.Empty) {
								dungeon[meHubP.x + (m * -horizontalSign), meHubP.y + (n * -verticalSign)].roomType = RoomType.Corridor;
								succesfullyCreated++;
							}
						}

						for(; m < absHDifference; m++) {
							if(	dungeon[meHubP.x + (m * -horizontalSign), connectToP.y].roomType == RoomType.Empty) {
								dungeon[meHubP.x + (m * -horizontalSign), connectToP.y].roomType = RoomType.Corridor;
								succesfullyCreated++;
							}
						}
					//}
				}

				nonMeHubs.RemoveAt(randomized);
			}
		}
	}
}