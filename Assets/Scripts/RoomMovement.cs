﻿using UnityEngine;
using System.Collections;

public class RoomMovement : MonoBehaviour {
	public enum RoomStatus {
		Down,
		GoingDown,
		Up,
		RisingUp
	}

	public RoomStatus initialState = RoomStatus.Down;
	public RoomStatus actualState { get; private set; }

	private Transform myTransform;

	public float timeToRise;
	private float timeCollector;

	public float downY, upY;

	void Awake() {
		actualState = initialState;
		myTransform = transform;
	}

	void Update() {
		Vector3 roomPosition;
		switch(actualState) {
			case RoomStatus.RisingUp:
				timeCollector += Time.deltaTime;

				roomPosition = myTransform.position;
				roomPosition.y = Mathf.SmoothStep(downY, upY, timeCollector / timeToRise);
				myTransform.position = roomPosition;

				if(timeCollector >= timeToRise)
					actualState = RoomStatus.Up;
				break;
			case RoomStatus.GoingDown:
				timeCollector += Time.deltaTime;
				
				roomPosition = myTransform.position;
				roomPosition.y = Mathf.SmoothStep(upY, downY, timeCollector / timeToRise);
				myTransform.position = roomPosition;
			
				if(timeCollector >= timeToRise)
					actualState = RoomStatus.Down;
				break;
		}
	}

	public void GoUp() {
		if(actualState == RoomStatus.Down) {
			actualState = RoomStatus.RisingUp;
			timeCollector = 0;
		}
	}
	public void GoDown() {
		if(actualState == RoomStatus.Up) {
			actualState = RoomStatus.GoingDown;
			timeCollector = 0;
		}
	}
}
